import React from "react"
import Layout from "../components/Layout"
import Link from "next/link"
import fetch from "isomorphic-unfetch"

const PostLink = props => (
  <div>
    <Link as={`/post/${props.id}`} href={`/post?id=${props.id}`}>
      <a>{props.title}</a>
    </Link>
  </div>
)

class Index extends React.Component {
  render() {
    return (
      <Layout>
        <div>Hello, Next.js!</div>
        <h1>Batman TV Shows</h1>
        {
          this.props.shows.map(show => (
            <PostLink 
              key={show.id} 
              id={show.id} 
              title={show.name}/>
          ))
        }
      </Layout>
    )
  }
}

Index.getInitialProps = async function() {
  const res = await fetch('https://api.tvmaze.com/search/shows?q=batman')
  const data = await res.json()

  return {
    shows: data.map(entry => entry.show)
  }
}

export default Index